#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
DB_FILE_NAME = "./test.db"

# making it a class object to ensure DB connection is released
# on object destruction
class myDBObject(object):
    """
    A class to hold the cursor and connection to SQLite DB

    Attributes:
        _db_connection: connection to sqlite db
        _db_cur: db cursor

    Methods:
        query: executes the query on the db
        fetch_all:  returns all the output after running the query()
        commit: ensures the data is pushed to DB
    """

    _db_connection = None
    _db_cur = None

    def __init__(self):
        if DB_FILE_NAME is None:
            raise Exception("DB_FILE_NAME can not be empty")
        self._db_connection = sqlite3.connect(DB_FILE_NAME)
        self._db_cur = self._db_connection.cursor()

    def query(self, query, parms=None):
        """
        takes the sqlite query and execute it on the cursor
        INPUT:
            query: sqlite query
            parms: parameters for sqlite query
        """
        if parms is None:
            return self._db_cur.execute(query)
        else:
            return self._db_cur.execute(query, parms)

    def fetch_all(self):
        """
        fetches the list of tuples returned by execution of query
        """
        return self._db_cur.fetchall()

    def commit(self):
        """
        commits the data into SQLite DB
        """
        return self._db_connection.commit()

    def __del__(self):
        """
        Helps by ensuring the DB connection is released when the
        object is destroyed
        """
        self._db_connection.close()
